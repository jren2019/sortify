# Use an official Python runtime as a base image
FROM python:3.6

MAINTAINER Jun Ren

ENV PYTHONUNBUFFERED 1

ARG uid=1000

# Install extra packages.
RUN apt-get update && apt-get install -y --no-install-recommends vim default-mysql-client libxml2-dev libxmlsec1-dev && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /

## Install any needed packages specified in requirements.txt
RUN pip install Cython numpy && pip install -r requirements.txt --no-cache-dir

RUN pip install colorama

# Make port 8000 available to the world outside this container
EXPOSE 8000

WORKDIR /code

COPY . .

ENTRYPOINT ["/bin/bash"]
CMD ["run_app.sh"]
