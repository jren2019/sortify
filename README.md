Test solution for Sortify.tm Limited - Intermediate Python Developer

Please go to "https://sortify.har.ac.nz" have a quick look for the solution.

# Local installation details:
# Run with docker in local environment

## Install Docker and Docker-compose
Follow the instruction here to install Docker version 19.03.1 and docker-compose version 1.24.1:

https://docs.docker.com/docker-for-mac/install/

https://docs.docker.com/compose/install/

## Now run
### start nginx container
Enter into the nginx-server and start the nginx container

```bash
cd sortify/nginx-server
docker-compose up --build -d
```


Enter into the root folder of the project "sortify" and run following command to start the docker containers: 
```bash
docker-compose up --build -d
```
Now you can visit 127.0.0.1 to see the demo page, and the links for the api is listed in the demo page.
If you met any external network cannot be found exception, please check the solution that listed in the end of this readme file. 


### demo: 

URL:  127.0.0.1/posts/wordsegmentation

Put your input strings in the upper box and then click submit button, the results returned by the server will be displayed in the lower box.

e.g.

lovesbeeraJavaScriptGymAcceptTokenAccept

the retruned results will be:

loves(Y) beer(N) a(Y) javascript(Y) gym(Y) accept(Y) token(Y) accept(Y)

The label in the bracket of each segmented word indicates the word is in blacklist or not, Y means Yes and N means No.




### api v1:

URL:  127.0.0.1/posts/wordsegmentation/api/v1

api_v1 page, return one string, the label is appended to each word
e.g.
put the folowing json data in the Content box and click post:
```json
{
   "input_string":"lovesbeeraJavaScriptGymAcceptTokenAccept"
}
```

the result is similar to the following, where the character in the bracket following each word indicates the word is 
blacklisted or not ('Y' means Yes, 'N" means No).:

```json
{
    "message": "Sortify.tm word segmentation data!",
    "target_text": "loves(Y) beer(N) a(Y) javascript(Y) gym(Y) accept(Y) token(Y) accept(Y)"
}
```

### api v2:

URL:  127.0.0.1/posts/wordsegmentation/api/v2

1) api_v2 page, return two lists

e.g.
put the folowing json data in the Content box and click post:
```json
{
   "input_string":"lovesbeeraJavaScriptGymAcceptTokenAccept"
}
```

the result is similar to the following format, two lists "target_text" and "blacklisted_or_not". THe target_text is the 
segmentated result for the input strings, the blacklisted_or_not is also a list, with same length of target_text. Each
element in blacklisted_or_not indicates the corresponding word in the target_text is blacklisted or not, Y means Yes and N means No:

```json
{
    "message": "Sortify.tm word segmentation data!",
    "target_text": "loves beer a javascript gym accept token accept",
    "blacklisted_or_not": "Y N Y Y Y Y Y Y"
}
```


# Run without docker in local environment
## Install C++ compiler
### On Mac
Install XCode

## Install python 3.6

Follow the instruction here: https://www.python.org/downloads/mac-osx/. 

Open a command line window (Windows) or a terminal and type in:
```bash
python --version
pip --version
```

You should see something similar to this, or else you need to fix the problem before moving to the next step.
```text
python 3.6.5
pip 9.0.2 from C:/Python36/Script (python 3.6)
```

## Install git
### On MacOS
**Open a terminal**

#### Step 1 – Install Homebrew
**Copy & paste the following** into the terminal window and **hit `Return`**.
```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew doctor
```
You will be offered to install the *Command Line Developer Tools* from *Apple*. **Confirm by clicking *Install***. After the installation finished, continue installing *Homebrew* by **hitting `Return`** again.

#### Step 2 – Install Git
**Copy & paste the following** into the terminal window and **hit `Return`**.
```bash
brew install git
```

## Install virtualenv
Open a terminal or cmd window and type:
```bash
pip install virtualenv
```

### Create a virtual environment:
> Note: if you use PyCharm, make sure you run this command BEFORE importing the project into Pycharm. Otherwise it will use the default interpreter and you'll have to change that

```bash
virtualenv .venv
```

### Run the project:
#### Source the virtual environment.
> Note: You only need to do this when you run commands from `cmd` or a terminal. On Pycharm, the virtualenv is automatically loaded.
>
##### On Linux and Mac
```bash
source .venv/bin/activate
```

### Now run
```bash
pip install -r requirements.txt
python manage.py migrate
```

# Production environment
please visit "https://sortify.har.ac.nz" for details

some pictures for the implemented api:

1) index page
![index page](docs/index_page.png)

2) demo page
Put your input strings in the upper box and then click submit button, the results returned by the server will be displayed in the lower box.

e.g.

lovesbeeraJavaScriptGymAcceptTokenAccept

the retruned results will be:

loves(Y) beer(N) a(Y) javascript(Y) gym(Y) accept(Y) token(Y) accept(Y)

The label in the bracket of each segmented word indicate the word is in blacklist or not, Y means Yes and N means No.


![demo page](docs/demo.png)

3) api_v1 page, return one string, the label is appended to each word
e.g.
put the folowing json data in the Content box and click post:
```json
{
   "input_string":"lovesbeeraJavaScriptGymAcceptTokenAccept"
}
```

the result is similar to the following, where the character in the bracket following each word indicates the word is 
blacklisted or not ('Y' means Yes, 'N" means No).:

```json
{
    "message": "Sortify.tm word segmentation data!",
    "target_text": "loves(Y) beer(N) a(Y) javascript(Y) gym(Y) accept(Y) token(Y) accept(Y)"
}
```

![api_v1 page](docs/api_v1.png)

4) api_v2 page, return two lists

e.g.
put the folowing json data in the Content box and click post:
```json
{
   "input_string":"lovesbeeraJavaScriptGymAcceptTokenAccept"
}
```

the result is similar to the following format, two lists "target_text" and "blacklisted_or_not". THe target_text is the 
segmentated result for the input strings, the blacklisted_or_not is also a list, with same length of target_text. Each
element in blacklisted_or_not indicates the corresponding word in the target_text is blacklisted or not, Y means Yes and N means No:

```json
{
    "message": "Sortify.tm word segmentation data!",
    "target_text": "loves beer a javascript gym accept token accept",
    "blacklisted_or_not": "Y N Y Y Y Y Y Y"
}
```


![api_v2 page](docs/api_v2.png)



# Exception
I used a external network: nginx-server_web, if some error about this occured as following picture, it might be the name of the network is changed by the docker.

![api_v2 page](docs/exception.png)

You need to check the network that created by the docker by runing command:
```bash
docker network ps
```

![api_v2 page](docs/exception_network.png)
In the above picture, you can see a network name similar to "nginx-server_web" or "nginxserver_web".

You can change the name of the networks in the "docker-compose.yml" to the name that is shown in the above picture.

If other exception occured, please let me know.


  


