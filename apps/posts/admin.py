from django.contrib import admin

# Register your models here.
from .models import WordSegmentation


class WordSegmentationAdmin(admin.ModelAdmin):
    list_display = ['input_sentence', 'output_sentence']
    fields = ['input_sentence', 'output_sentence']


admin.site.register(WordSegmentation, WordSegmentationAdmin)
