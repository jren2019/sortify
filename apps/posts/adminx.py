import xadmin
from xadmin import views
from .models import WordSegmentation


@xadmin.sites.register(WordSegmentation)
class WordSegmentationAdmin(object):
    model_icon = 'fa fa-home'
    list_display = ('input_sentence', 'output_sentence')
    search_fields = ['input_sentence', 'output_sentence']


class GlobalSetting(object):
    menu_style = "accordion"
    site_title = 'SORTIFY'
    site_footer = 'SORTIFY'


class BaseSetting(object):
    enable_themes = True
    use_bootswatch = True


xadmin.site.register(views.BaseAdminView, BaseSetting)
xadmin.site.register(views.CommAdminView, GlobalSetting)
