from django import forms


class WordSegmentationForm(forms.Form):
    original_text = forms.CharField(required=True,
                                    label='Your orginal article',
                                    max_length=5000,
                                    widget=forms.Textarea(attrs={'width': "100%", 'cols': "80", 'rows': "15", }),
                                    help_text="Original article")

    def clean(self):
        cleaned_data = super(WordSegmentationForm, self).clean()
        original_text = cleaned_data.get('original_text')

        if not original_text:
            raise forms.ValidationError('original_text cannot be empty!')
