from datetime import datetime

from django.db import models


class WordSegmentation(models.Model):
    """
    list all the papers/videos that published on the specified platform
    """
    input_sentence = models.CharField(default="", max_length=500, verbose_name="input_sentence", help_text="input_sentence")
    output_sentence = models.CharField(default="", max_length=600, verbose_name="output_sentence", help_text="output_sentence")

    class Meta:
        verbose_name = "WordSegmentation "
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.output_sentence

