#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = "jren"
# Email: jun.ren.nz@gmail.com

from rest_framework import serializers

from .models import WordSegmentation


class WordSegmentationSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        many = kwargs.pop('many', True)
        super(WordSegmentationSerializer, self).__init__(many=many, *args, **kwargs)

    class Meta:
        model = WordSegmentation
        fields = (['input_sentence', 'output_sentence'])
