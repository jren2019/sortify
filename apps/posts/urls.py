#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = "jren"
# Email: jun.ren.nz@gmail.com


from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework.authtoken import views as drf_views

from . import views

router = DefaultRouter()
# kept this for other api
# http://127.0.0.1:8000/sortify/api/
router.register(r'api', views.WordSegmentationViewSet, base_name="WordSegmentationSerializer")


urlpatterns = [
    path(r'sortify/', include(router.urls)),
    path('', views.index, name='index'),
    path(r'auth', drf_views.obtain_auth_token, name='auth'),
    path('wordsegmentation', views.wordsegmentation, name='wordsegmentation'),
    path('wordsegmentation/api/v1', views.wordsegmentationApiv1, name='wordsegmentation'),     # http://127.0.0.1:8000/posts/wordsegmentation/api/v1
    path('wordsegmentation/api/v2', views.wordsegmentationApiv2, name='wordsegmentation'),     # http://127.0.0.1:8000/posts/wordsegmentation/api/v2
]
