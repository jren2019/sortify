from django.http import HttpResponse
from rest_framework import status
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from apps.posts.forms import WordSegmentationForm
from .models import WordSegmentation
from .serializers import WordSegmentationSerializer
from datetime import datetime

from django.shortcuts import render
from rest_framework.decorators import api_view

from django.conf import settings
from wordsegment import load, segment
load()


class PostsSetPagination(PageNumberPagination):
    page_size = 19
    page_size_query_param = 'page_size'
    max_page_size = 10000


def check_blacklisted(target_text_segmented):
    blacklisted_or_not = []

    for i in range(len(target_text_segmented)):
        blacklisted_tmp = 'Y' if target_text_segmented[i] in settings.BLACKLISTEDWORDS else 'N'
        blacklisted_or_not.append(blacklisted_tmp)

    return blacklisted_or_not

# {
#   "input_string":"lovesbeeraJavaScriptGymAcceptToken"
# }

@api_view(['GET', 'POST'])
def wordsegmentationApiv1(request):
    if request.method == 'POST':

        original_text = request.data

        target_text_segmented = segment(original_text["input_string"])
        blacklisted_or_not = check_blacklisted(target_text_segmented)
        annotated_target_text_segmented = [m + "(" + n + ")" for m, n in zip(target_text_segmented, blacklisted_or_not)]

        target_text = ' '.join(annotated_target_text_segmented)
        return Response({"message": "Sortify.tm word segmentation data!", "target_text": target_text})

    return Response({"message": "Sortify.tm please submit your data with post"})


@api_view(['GET', 'POST'])
def wordsegmentationApiv2(request):
    if request.method == 'POST':

        original_text = request.data

        target_text_segmented = segment(original_text["input_string"])
        blacklisted_or_not = check_blacklisted(target_text_segmented)

        target_text = ' '.join(target_text_segmented)
        target_text_blocked_or_not = ' '.join(blacklisted_or_not)

        return Response({"message": "Sortify.tm word segmentation data!", "target_text": target_text, "blacklisted_or_not": target_text_blocked_or_not})

    return Response({"message": "Sortify.tm please submit your data with post"})


# http://127.0.0.1:8000/posts/wordsegmentation
def wordsegmentation(request):
    #
    # print(request.method)
    # return HttpResponse("Hello, world. You're at the content list index.")
    """
    submit the original text to the backend and return the paraphrased text to the frontend
    """
    if request.method == 'POST':
        form = WordSegmentationForm(request.POST)
        if form.is_valid():
            original_text = form.cleaned_data['original_text']
            target_text_segmented = segment(original_text)
            blacklisted_or_not = check_blacklisted(target_text_segmented)
            annotated_target_text_segmented = [m + "(" + n + ")" for m, n in zip(target_text_segmented, blacklisted_or_not)]
            target_text = ' '.join(annotated_target_text_segmented)

            return render(request, 'posts/wordsegmentation.html', {'form': form, 'target_text': target_text})
    else:
        form = WordSegmentationForm()

    return render(request, 'posts/wordsegmentation.html', {'form': form})


def index(request):
    return HttpResponse("Hello, world. sortify.tm.")


# other apis
class WordSegmentationViewSet(viewsets.ModelViewSet):
    serializer_class = WordSegmentationSerializer
    pagination_class = PostsSetPagination

    def get_queryset(self):
        pingtai = self.request.GET.get('pingtai')

        if pingtai is not None and pingtai != "":
            queryset = WordSegmentation.objects.filter(platform_type=pingtai)
        else:
            queryset = WordSegmentation.objects.all()

        return queryset