# from django.contrib import admin
# from django.contrib.auth.admin import UserAdmin
# from django.utils.translation import ugettext_lazy as _
#
# from .models import User
#
#
# class CustomUserAdmin(UserAdmin):
#     list_display = ('id', 'email', 'created', 'modified')
#     list_filter = ('is_active', 'is_staff', 'groups')
#     search_fields = ('email',)
#     ordering = ('email',)
#     filter_horizontal = ('groups', 'user_permissions',)
#
#     fieldsets = (
#         (None, {'fields': ('email', 'password')}),
#         (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
#                                        'groups', 'user_permissions')}),
#     )
#     add_fieldsets = (
#         (None, {
#             'classes': ('wide',),
#             'fields': ('email', 'password1', 'password2')}),
#     )


# admin.site.register(User, CustomUserAdmin)

from django.apps import apps
from django.contrib import admin

from users.admin_utils import generate_admin_class

app = apps.get_app_config('users')

for model_name, model in app.models.items():
    model_admin_class = generate_admin_class(model)
    admin.site.register(model, model_admin_class)
