from django.apps import AppConfig


class UsersConfig(AppConfig):
    name = 'users'

    def ready(self):
        pass
        # from users.utils import SendEmailThread
        # send_email_thread = SendEmailThread("Hello 123456", "forget-password", ['jun.ren.nz@gmail.com', ], context={})
        # send_email_thread.start()
