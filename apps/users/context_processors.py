from django.conf import settings # import the settings file


def site_init(request):
    '''
    credit: https://stackoverflow.com/questions/433162/can-i-access-constants-in-settings-py-from-templates-in-django
    :param request:
    :return:
    '''
    # return the value you want as a dictionnary. you may add multiple values in there.
    return {'SITE_URL': settings.SITE_URL}
