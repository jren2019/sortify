#! /usr/bin/env python
# -*- coding: utf-8 -*-
# __author__ = "jren"
# Email: jun.ren.nz@gmail.com



from django.urls import path
from .views import current_user, UserList

urlpatterns = [
    path('current_user/', current_user),
    path('users/', UserList.as_view())
]
