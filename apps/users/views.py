from django.shortcuts import render  # noqa
#
#
# # Create your views here.
# from django.contrib.auth import get_user_model
# from .serializers import UserSerializer
# from rest_framework import viewsets
#
# User = get_user_model()
#
#
# class UserViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#
#     queryset = User.objects.all().order_by('-date_joined')
#     serializer_class = UserSerializer


from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import UserSerializer, UserSerializerWithToken

# from django.contrib import auth
from django.http import HttpResponse, JsonResponse
from rest_framework import permissions
from rest_framework.decorators import permission_classes


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def current_user(request):
    """
    Determine the current user by their token, and return their data
    """
    # permission_classes = (permissions.AllowAny,)

    serializer = UserSerializer(request.user)
    print(serializer.data)

    # authenticated_user = auth.authenticate(request, username=serializer.data['username'], password=serializer.data['password'])
    # auth.login(request, authenticated_user)
    # return Response(serializer.data)
    return JsonResponse(serializer.data, safe=False)


class UserList(APIView):
    """
    Create a new user. It's called 'UserList' because normally we'd have a get
    method here too, for retrieving a list of all User objects.
    """

    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        pass
