import React from 'react';
import {Row, Col} from 'antd'
import ReactDOM from "react-dom";
import NavLeft from './../../pages/components/NavLeft'
import Header from "./../../pages/components/Header";
import Footer from "./../../pages/components/Footer";
import Home from "./../../pages/Home";

import './theme.less';

class App extends React.Component{
    render(){
        return (
            <Row className="container">
                <Col span={3} className="nav-left">
                    <NavLeft/>

                </Col>
                <Col span={21} className="main">
                    <Header/>
                    <Row className="content">
                        <Home/>
                    </Row>
                    <Footer/>
                </Col>
            </Row>
        )
    }
}

export default App
