import React from 'react'
import './index.scss'
export default class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
               版权所有：慕课网&HuoXingXing（推荐使用谷歌浏览器，可以获得更佳操作页面体验） 技术支持：HuoXingXing
            </div>
        );
    }
}
