import React from 'react'
import MenuConfig from './../../config/menuConfig'
import { NavLink } from 'react-router-dom'
import { Menu, Icon } from 'antd';
import './index.scss';

const SubMenu = Menu.SubMenu;

export default class NavLeft extends React.Component{
    componentWillMount(){
        const menuTreeNode = this.renderMenu(MenuConfig);

        this.setState({
            menuTreeNode
        })
    };
    renderMenu(data){
        return data.map((item)=>{
            if(item.children){
                return (
                    <SubMenu title={item.title} key={item.key}>
                        { this.renderMenu(item.children)}
                    </SubMenu>
                )
            }
            return <Menu.Item title={item.title} key={item.key}>
                {item.title}
            </Menu.Item>
        })
    };


    render(){
        return (
            <div>
                <div className="logo">
                    <img src={"/assets/images/logo-ant.svg"} alt={""}/>
                    <h1>HuoXingXingZiXun</h1>
                </div>
                <Menu
                    theme={"dark"}
                >
                    {this.state.menuTreeNode}
                </Menu>

            </div>
        )
    }
}
