// import 'pages/admin';
import '../sass/style.scss';
import React from 'react';
import Router from './routes'
import ReactDOM from "react-dom";

ReactDOM.render(<Router/>, document.getElementById('react-app'));

import $ from 'jquery';
global.jQuery = $;
global.$ = $;
//
import 'bootstrap';