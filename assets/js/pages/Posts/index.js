import React from 'react'
import {Card, Row, Button, Input, Col, Table, Form, Select, DatePicker} from 'antd'

import axios from './../../axios/index'
import Utils from './../utils/utils';


const Search = Input.Search;
const FormItem = Form.Item;
const Option = Select.Option;

export default class Posts extends React.Component{

    constructor(props){
        super(props);
        this.state={};
        this.params = {
            page:1
        };

        this.requestReadNum = this.requestReadNum.bind(this);
        this.test_2 = this.test_2.bind(this);
        this.handleFilter = this.handleFilter.bind(this);


    }

    componentDidMount() {

        let LingyuID = this.props.match.params.LingyuID;
        console.log(LingyuID);  //get the postID 123, then we can use the id to query more details from the database
        if(LingyuID){
            this.requestLingyu(LingyuID);
        }
        else {
            this.request();
        }

        // hashHistory.push({
        //     pathname: '/blank',
        // });
        //
        // hashHistory.push({
        //     pathname: `/ly_posts_ly/${LingyuID}`,
        // })
    }

    request(){
        let _this = this;
        axios.ajax({
            url:'/posts/ContentItems/op_items/',
            data:{
                params:{
                    page:this.params.page
                },
                // isShowLoading:false
            }
        }).then((res)=>{
            // console.log(JSON.stringify((res)));
            res.results.map((item, index) => {
                    item.key = index;
                });
            // console.log(res.count);
            // console.log(res.next);
            // console.log(res.previous);

            this.setState({
                    dataSource:res.results,
                    pagination:Utils.pagination(res, (current)=>{
                        //to-do
                        _this.params.page=current;
                        this.request();
                    })
                })
        })
    }

    request22(){
        let _this = this;
        axios.ajax({
            url:'/posts/ContentItems/op_items_com/',
            data:{
                params: this.params,
                // isShowLoading:false
            }
        }).then((res)=>{
            // console.log(JSON.stringify((res)));
            res.results.map((item, index) => {
                    item.key = index;
                });
            // console.log(res.count);
            // console.log(res.next);
            // console.log(res.previous);

            this.setState({
                    dataSource:res.results,
                    pagination:Utils.pagination(res, (current)=>{
                        //to-do
                        _this.params.page=current;
                        this.request();
                    })
                })
        })
    }

    requestLingyu(LingyuID){
        let _this = this;
        axios.ajax({
            url:'/posts/ContentItems/op_items_ly/',
            data:{
                params:{
                    page:this.params.page,
                    LingyuID:LingyuID,
                },
                // isShowLoading:false
            }
        }).then((res)=>{
            // console.log(JSON.stringify((res)));
            res.results.map((item, index) => {
                    item.key = index;
                });
            // console.log(res.count);
            // console.log(res.next);
            // console.log(res.previous);

            this.setState({
                    dataSource:res.results,
                    pagination:Utils.pagination(res, (current)=>{
                        //to-do
                        _this.params.page=current;
                        this.request();
                    })
                })
        })
    }

    requestReadNum(num){
        let _this = this;
        axios.ajax({
            url:'/posts/ContentItems/op_items_yuedu/',
            data:{
                params:{
                    page:this.params.page,
                    read_num:num,
                },
                // isShowLoading:false
            }
        }).then((res)=>{
            // console.log(JSON.stringify((res)));
            res.results.map((item, index) => {
                    item.key = index;
                });
            // console.log(res.count);
            // console.log(res.next);
            // console.log(res.previous);

            this.setState({
                    dataSource:res.results,
                    pagination:Utils.pagination(res, (current)=>{
                        //to-do
                        _this.params.page=current;
                        this.request();
                    })
                })
        })
    }

    test_2(num){
        console.log(num)
    }


    handleFilter(params){
        console.log(params)
        this.params = params;
        // console.log("got the params");
        // console.log(params);
        // //this.requestList();
        // this.test_2(888)
        this.request22();
    }

    render(){
        const columns = [
            {
                title:'id',
                dataIndex:'title'
            },
            {
                title: '用户名',
                dataIndex: 'url'
            },
            {
                title: '性别',
                dataIndex: 'read_num',
            },
            {
                title: '状态',
                dataIndex: 'commnent_num',
            },
            {
                title: '爱好',
                dataIndex: 'platform_type',
                render(platform_type){
                    let config = {
                        '1':'趣头条',
                        '2':'今日头条',
                    };
                    return config[platform_type]
                }
            },
            {
                title: '生日',
                dataIndex: 'pub_time'
            },
            {
                title: '状态',
                dataIndex: 'is_available',
                render(is_available){
                    return is_available==1 ? 'Y' : 'N'
                }
            },
            {
                title: '早起时间',
                dataIndex: 'content_type'
            }
        ];

        return (
            <div style={{color:'blue'}}>
                <Row className={"breadcrumb"}>
                    <Col span={24}>
                        <Search
                          placeholder="关键字搜索"
                          onSearch={value => console.log(value)}
                          enterButton
                        />
                    </Col>
                </Row>
                <Card>
                    <FilterForm filterSubmit={this.handleFilter}/>
                </Card>
                <div className="content-wrap">
                    <Table
                    bordered
                    columns={columns}
                    dataSource={this.state.dataSource}
                    pagination={this.state.pagination}
                    // scroll={{y:800}}
                    />
                </div>
            </div>
        );
    }

}


class FilterForm extends React.Component{

    constructor(props){
        super(props);
        this.state={
            testthis:'77777777777'
        }

        this.requestfilter = this.requestfilter.bind(this);
        this.reset = this.reset.bind(this);
        this.testClick = this.testClick.bind(this);

        this.requestReadNum = this.requestReadNum.bind(this);
        this.test_2 = this.test_2.bind(this);
    }

    testClick(){
        console.log("test");
        let postID='xxxyyytest';
        window.open(`/#/detail/${postID}`, '_blank');
    }

    requestReadNum(num){
        let _this = this;
        axios.ajax({
            url:'/posts/ContentItems/op_items_yuedu/',
            data:{
                params:{
                    page:this.params.page,
                    read_num:num,
                },
                // isShowLoading:false
            }
        }).then((res)=>{
            // console.log(JSON.stringify((res)));
            res.results.map((item, index) => {
                    item.key = index;
                });
            // console.log(res.count);
            // console.log(res.next);
            // console.log(res.previous);

            this.setState({
                    dataSource:res.results,
                    pagination:Utils.pagination(res, (current)=>{
                        //to-do
                        _this.params.page=current;
                        this.request();
                    })
                })
        })
    }

    test_2(num){
        console.log(num)
    }

    requestfilter(){
        console.log(this.state.testthis);
        let forminfo = this.props.form.getFieldsValue();
        // console.log(forminfo);
        // let xx=20;
        this.props.filterSubmit(forminfo);

    };

    reset(){
        this.props.form.resetFields();
    };

    render(){
        const { getFieldDecorator } = this.props.form;
        return (
            <Form layout="inline">
                <FormItem label="平台">
                    {
                        getFieldDecorator('pingtai')(
                            <Select
                                style={{width:95}}
                                placeholder="全部"
                            >
                                <Option value="">全部</Option>
                                <Option value="1">大鱼号</Option>
                                <Option value="2">企鹅号</Option>
                                <Option value="3">趣头条</Option>
                                <Option value="4">百家号</Option>
                                <Option value="5">网易号</Option>
                                <Option value="6">搜狐号</Option>
                                <Option value="7">新浪号</Option>
                                <Option value="8">今日头条</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label="类型">
                    {
                        getFieldDecorator('leixing')(
                            <Select
                                style={{width:70}}
                                placeholder="文章"
                            >
                                <Option value="1">文章</Option>
                                <Option value="2">视频</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label="领域">
                    {
                        getFieldDecorator('Lingyu')(
                            <Select
                                style={{width:70}}
                                placeholder="领域"
                            >
                                <Option value="">全部</Option>
                                <Option value="1">娱乐</Option>
                                <Option value="2">军事</Option>
                                <Option value="3">国际</Option>
                                <Option value="4">新闻</Option>
                                <Option value="5">社会</Option>
                                <Option value="6">历史</Option>
                                <Option value="7">育儿</Option>
                                <Option value="8">情感</Option>
                                <Option value="9">体育</Option>
                                <Option value="10">健康</Option>
                                <Option value="11">财经</Option>
                                <Option value="12">三农</Option>
                                <Option value="13">故事</Option>
                                <Option value="14">汽车</Option>
                                <Option value="15">美食</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label="阅读">
                    {
                        getFieldDecorator('read_num')(
                            <Select
                                style={{width:70}}
                                placeholder="全部"
                            >
                                <Option value="">全部</Option>
                                <Option value="20">50000以上</Option>
                                <Option value="2">20000以上</Option>
                                <Option value="3">5000以上</Option>
                            </Select>
                        )
                    }
                </FormItem>
                <FormItem label="时间">
                    {
                        getFieldDecorator('start_date')(
                            <DatePicker format="YYYY-MM-DD" style={{width:120}}/>
                        )
                    }
                </FormItem>
                <FormItem label="~" colon={false}>
                    {
                        getFieldDecorator('edn_date')(
                            <DatePicker format="YYYY-MM-DD" style={{width:120}}/>
                        )
                    }
                </FormItem>
                <FormItem>
                    <Button type="primary" style={{margin:'0 2px'}} onClick={this.requestfilter}>查询</Button>
                    {/*<Button style={{margin:'0 5px'}} onClick={this.reset}>重置</Button>*/}
                    <Button style={{margin:'0 2px'}}>导出</Button>
                    {/*<Button style={{margin:'0 10px'}} onClick={this.testClick}>Test</Button>*/}
                </FormItem>
            </Form>
        );
    }
}
FilterForm = Form.create({})(FilterForm);
