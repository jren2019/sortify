import React from 'react';
import {Row, Col} from 'antd'

import NavLeft from './components/NavLeft'
import Header from "./components/Header";
import Footer from "./components/Footer";

import './theme.less';

class Admin extends React.Component{
    render(){
        return (
            <div className="container">
                <NavLeft/>
                <Header/>
                <div className="content">
                    {this.props.children}
                </div>
                <Footer/>
            </div>
        )
    }
}

export default Admin


