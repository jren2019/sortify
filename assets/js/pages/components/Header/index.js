import React from 'react'
import { Row,Col,Spin } from "antd"
import './index.scss'
import Util from '../../utils/utils.js'
import axios from '../../../axios'



export default class Header extends React.Component{
    componentWillMount(){
        this.setState({
            userName:'星火一角'
        });
        setInterval(()=>{
            let time = new Date().getTime();
            let sysTime = Util.formateDate(time);
            this.setState({
                sysTime
            })
        },1000);
        this.getWeatherAPIData();
    }

    getWeatherAPIData(){
        let city = '北京';
        axios.jsonp({
            url:'http://api.map.baidu.com/telematics/v3/weather?location='+encodeURIComponent(city)+'&output=json&ak=3p49MVra6urFRGOT9s8UBWr2'
        }).then((res)=>{
            if(res.status == 'success'){
                let data = res.results[0].weather_data[0];
                this.setState({
                    dayPictureUrl:data.dayPictureUrl,
                    weather:data.weather
                })
            }
        })
    }

    render(){
        const menuType = this.props.menuType;

        return (
            <div className={"header"}>
                {
                        menuType?'':
                            <Row className={"header-top"}>
                                <Col span={4} style={{color:"black"}} className={"breadcrumb-title"}>数据同步中<Spin size="large"/></Col>
                                <Col span={10} style={{color:"black"}} className={"weather"}>
                                    <span className={"date"}>{this.state.sysTime}</span>
                                    <span className="weather-img">
                                        <img src={this.state.dayPictureUrl} alt="" />
                                    </span>
                                    <span className={"weather-detail"}>{this.state.weather}</span>
                                </Col>
                                <Col span={9}>
                                    <span style={{color:"black"}}>欢迎， {this.state.userName}</span>
                                    <a href={"#"}>退出</a>
                                </Col>
                            </Row>
                    }

            </div>
        )
    }
}
