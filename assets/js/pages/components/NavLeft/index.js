import React from 'react'
import MenuConfig from './../../config/menuConfig'
import { NavLink} from 'react-router-dom'
import { Menu, Icon, Row, Col } from 'antd';
import './index.scss';

const SubMenu = Menu.SubMenu;

export default class NavLeft extends React.Component{
    componentWillMount(){
        const menuTreeNode = this.renderMenu(MenuConfig);

        this.setState({
            menuTreeNode
        })
    };
    renderMenu(data){
        return data.map((item)=>{
            if(item.children){
                return (
                    <SubMenu title={item.title} key={item.key}>
                        { this.renderMenu(item.children)}
                    </SubMenu>
                )
            }

            return <Menu.Item title={item.title} key={item.key}>
                    <NavLink to={item.key}>{item.title}</NavLink>
            </Menu.Item>
        })
    };


    render(){
        return (
            <Row className="nav-left">
                <Col className="logo" span={1}>
                    <img src={"/static/images/logo-ant.svg"} alt={""}/>
                    {/*<h1>HuoXingXing</h1>*/}
                </Col>
                <Col>
                <Menu
                    theme={"dark"}
                    mode="horizontal"
                    span={23}
                >
                    {this.state.menuTreeNode}
                </Menu>
                </Col>

            </Row>
        )
    }
}
