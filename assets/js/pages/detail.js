import React from 'react';
import {Row, Col} from 'antd'

import NavLeft from './components/NavLeft'
import Header from "./components/Header";
import Footer from "./components/Footer";

import './theme.less';

class Detail extends React.Component{
    render(){
        return (
            <div>
                <Row className={"simple-page"}>
                    <NavLeft/>
                    <Header menuType="second"/>
                </Row>
                <Row className={"content"}>
                    {this.props.children}
                </Row>
                <Row>
                    <Footer/>
                </Row>
            </div>
        )
    }
}

export default Detail


