import React from 'react'
import axios from './../../axios/index'
import Utils from './../utils/utils';


export default class Postanalysis extends React.Component{

    constructor(props){
        super(props);
        this.state={
            testdata:"555555555",
            xxxtest_object:{},
            PostInfo:{}
        };
        this.params = {
            postID:""
        };
    }

    componentDidMount(){
        let postID = this.props.match.params.postID;
        console.log(postID);  //get the postID 123, then we can use the id to query more details from the database
        if(postID){
            this.request(postID);
            this.getDetailInfo(postID);
        }
    }

    request(postID){
        let _this = this;
        axios.ajax({
            url:'/posts/ContentItems/op_items_detail/',
            data:{
                params:{
                    postID:postID
                },
                // isShowLoading:false
            }
        }).then((res)=>{
            console.log(JSON.stringify((res)));
            res.results.map((item, index) => {
                    item.key = index;
                });
            // console.log(res.count);
            // console.log(res.next);
            // console.log(res.previous);
            let xxy = [11,22,33];
            let xxy_object = {"mm":"mmm111", "nn":"nnn111"}
            this.setState({
                    dataSource:res.count,
                    PostInfo:res.results[0]["url"],
                    xxxtest:xxy,
                    xxxtest_object:xxy_object
                });
            console.log(_this.state.dataSource);
            // console.log(_this.state.dataSource[0]["postID"]);
        })
    }

    getDetailInfo(postID){
        let _this = this;
        axios.ajax({
            url:'/posts/ContentItems/op_items_detail/',
            data:{
                params:{
                    postID:postID
                }
            }
        }).then((res)=>{
            _this.setState({
                    PostInfo:res.results[0],
                })
        })
        // console.log(_this.state.PostInfo);
        // console.log(_this.state.PostInfo[0]["postID"]);

    }

    render(){

        return (
            <div>
                This is Postanalysis page, the detail infomation for one post.
                <div>{this.state.testdata}</div>
                <div>{this.state.dataSource}</div>
                <div>{this.state.xxxtest}</div>
                <div>{this.state.xxxtest_object['mm']}</div>

                <div>{this.state.PostInfo["postID"]}</div>
                <div>{this.state.PostInfo["category_type"]}</div>
                <div>{this.state.PostInfo["commnent_num"]}</div>
                <div>{this.state.PostInfo["platform_type"]}</div>
                <div>{this.state.PostInfo["read_num"]}</div>
                <div>{this.state.PostInfo["pub_time"]}</div>
                <div>{this.state.PostInfo["title"]}</div>
                <div>{this.state.PostInfo["url"]}</div>

            </div>
        );
    }

}
