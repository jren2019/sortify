import React from 'react'
import { HashRouter, Route, Switch, Redirect} from 'react-router-dom'
import App from './../App'
import Login from './../pages/login'
import Admin from './../pages/admin'
import Posts from './../pages/Posts'
import Home from './../pages/Home'
import NoMatch from './../pages/nomatch'
import Detail from "../pages/detail";
import Postanalysis from "../pages/postanalysis";

export default class ERouter extends React.Component {
    render() {
        return (
            <HashRouter>
                <App>
                    {/*http://127.0.0.1:8000/#/login*/}
                    <Route path="/login" component={Login}/>
                    <Route path="/admin" render={()=>
                        <Admin>
                            <Switch>
                                <Route path={"/admin/home"} component={Home}/>
                                {/*http://127.0.0.1:8000/#/admin/posts_all*/}
                                {/*<Route path={"/admin/posts_all"} component={Posts}/>*/}
                                <Route path={"/admin/posts_all"} component={Home}/>
                                {/*<Route path={"/admin/ly_posts_ly/:LingyuID"} component={Posts}/>*/}
                                <Route component={NoMatch}/>
                            </Switch>
                        </Admin>
                    }
                    />
                    <Route path={"/ly_posts_ly"} render={()=>
                            <Admin>
                                <Switch>
                                    <Route path={"/ly_posts_ly/:LingyuID"} component={Posts}/>
                                    <Route component={NoMatch}/>
                                </Switch>
                            </Admin>
                        }
                    />
                    <Route path={"/detail"} render={()=>
                            <Detail>
                                <Switch>
                                    <Route path={"/detail/:postID"} component={Postanalysis}/>
                                    <Route component={NoMatch}/>
                                </Switch>
                            </Detail>
                        }
                    />

                </App>
            </HashRouter>
        );
    }
}
