# https://docs.djangoproject.com/en/1.10/ref/settings/
import os
import sys
import datetime
import traceback
from json import JSONEncoder
from uuid import UUID
import numpy as np
import dj_database_url
from maintenance import get_config
import string

envconf = get_config()
BASE_DIR = envconf['base_dir']
os.environ['PYTHONPATH'] = BASE_DIR
SITE_ID = 1
SECRET_KEY = envconf['secret_key']

sys.path.insert(0, BASE_DIR)
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))
sys.path.insert(0, os.path.join(BASE_DIR, 'extra_apps'))


def base_dir_join(*args):
    return os.path.join(BASE_DIR, *args)


DEBUG = os.environ.get('DEBUG', None)
if DEBUG:
    DEBUG = DEBUG == 'true'
else:
    DEBUG = envconf['debug']

ALLOWED_HOSTS = envconf['allowed_hosts']

# SECURE_HSTS_PRELOAD = True  # ask ask

ADMINS = (
    ('Admin', 'jun.ren.nz@gmail.com'),
)

AUTH_USER_MODEL = 'users.User'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',

    'xadmin',
    'crispy_forms',
    'reversion',

    'django_js_reverse',
    'webpack_loader',
    'import_export',


    'common',
    'apps.users',
    'apps.posts',

    'rest_framework',
    'rest_framework.authtoken',
    'widget_tweaks',
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'sortify.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [base_dir_join('templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'

# User uploaded content
MEDIA_URL = '/user_data/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'user_data')


WSGI_APPLICATION = 'sortify.wsgi.application'

REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.BrowsableAPIRenderer',
        'rest_framework.renderers.JSONRenderer',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 5,

    'DEFAULT_PERMISSION_CLASSES': [
        # 'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],

    'DEFAULT_MODEL_SERIALIZER_CLASS': 'rest_framework.serializers.HyperlinkedModelSerializer',

    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    )

}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en'

TIME_ZONE = 'Asia/Shanghai'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATICFILES_DIRS = (
    base_dir_join('assets'),
)

# Webpack
WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': False,  # on DEBUG should be False
        'STATS_FILE': base_dir_join('webpack-stats.json'),
        'POLL_INTERVAL': 0.1,
        'IGNORE': ['.+\.hot-update.js', '.+\.map']
    },
    'JQUERY': {
        'BUNDLE_DIR_NAME': 'bundles/',
        'STATS_FILE': 'jquery-webpack-stats.json',
    }
}

MAX_FILE_NAME_LENGTH = 20
DATA_UPLOAD_MAX_NUMBER_FIELDS = 1000

# Redis cache
_cache_config = envconf.get('cache', None)
if _cache_config:
    CACHES = {
        'default': {
            'BACKEND': _cache_config['backend'],
            'LOCATION': _cache_config['location'],
            'OPTIONS': _cache_config['options']
        }
    }


# Celery STUFF
# _broker_config = envconf['broker']
# CELERY_BROKER_URL = _broker_config['location']
# CELERY_RESULT_BACKEND = _broker_config['location']
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = envconf['timezone']
CELERY_SEND_TASK_ERROR_EMAILS = True

LOGIN_URL = '/login'

# site configuration
SITE_URL = envconf['site_url']
SITE_URL_BARE = SITE_URL[:-1]
assert SITE_URL.endswith('/'), 'SITE_URL must have trailing /'

EMAIL_CONFIG = envconf['email_config']
if EMAIL_CONFIG == 'console':
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    FROM_EMAIL = ''
else:  # assume we have fully-specified smtp configuration
    email_host, email_user, email_pass, email_port = EMAIL_CONFIG.split(':', 3)
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
    EMAIL_HOST = email_host
    EMAIL_HOST_USER = email_user
    EMAIL_HOST_PASSWORD = email_pass
    EMAIL_TIMEOUT = 10  # hopefully this is enough...
    EMAIL_PORT = email_port
    EMAIL_USE_TLS = True
    FROM_EMAIL = envconf['from_email']

DATABASES = {
    'default': dj_database_url.parse(envconf['database_url'])
}

JSONEncoder_olddefault = JSONEncoder.default


def JSONEncoder_newdefault(self, obj):
    """
    The original JSONEncoder doesn't handle datetime object.
    Replace it with this
    :param self:
    :param obj:
    :return: the JSONified string
    """
    if isinstance(obj, datetime.datetime):
        return obj.strftime(TIME_INPUT_FORMAT)
    elif isinstance(obj, datetime.date):
        return obj.strftime(DATE_INPUT_FORMAT)
    return JSONEncoder_olddefault(self, obj)


JSONEncoder.default = JSONEncoder_newdefault

AUDIO_COMPRESSED_FORMAT = 'mp4'

TZ_DETECT_COUNTRIES = ('NZ', 'AU', 'GB', 'US', 'CA', 'CN', 'JP', 'FR', 'DE')

CSRF_TRUSTED_ORIGINS = envconf['csrf_trusted_origin']

DATE_INPUT_FORMAT = '%Y-%m-%d'
TIME_INPUT_FORMAT = '%Y-%m-%d %H:%M:%S %z%Z'


def to_lower_case(word):
    # construct auxilliary dictionary to avoid ord and chr built-in methods
    upper_to_lower = {}
    for index in range(len(string.ascii_uppercase)): # this is 26 we can use it as constant though
        upper_to_lower[string.ascii_uppercase[index]] = string.ascii_lowercase[index]

    result = ''
    for alphabet in word:
        if alphabet in string.ascii_uppercase:
            result += upper_to_lower[alphabet]
        else:
            result += alphabet

    return result

BLACKLISTEDWORDS = [to_lower_case(x) for x in ['PHP', 'JavaScript', 'Microsoft', 'Windows', 'MSSQL', 'Healthy', 'Gym', 'Running', 'Authorization', 'Accept', 'Loves', 'Hate', 'Number', 'Token', 'Nails', 'The', 'A', 'Smaller', 'Bigger' 'Huge', 'Tiny']]

ERROR_TRACKER = None

# For local run:
if DEBUG:
    HOST = 'http://localhost:8000'

    # we whitelist localhost:3000 because that's where frontend will be served
    CORS_ORIGIN_WHITELIST = (
        'localhost:3000/'
    )

    DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'
    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
    # STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'

    STATICFILES_DIRS = (
        base_dir_join('assets'),  # ask ask
    )

    AUTH_PASSWORD_VALIDATORS = []  # allow easy passwords only on local

    # Celery
    CELERY_TASK_ALWAYS_EAGER = True

    # Email
    INSTALLED_APPS += ('naomi',)
    EMAIL_BACKEND = 'naomi.mail.backends.naomi.NaomiBackend'
    EMAIL_FILE_PATH = base_dir_join('tmp_email')

    # django-debug-toolbar and django-debug-toolbar-request-history
    INSTALLED_APPS += ('debug_toolbar',)
    MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
    INTERNAL_IPS = ['127.0.0.1', '::1']

    DEBUG_TOOLBAR_PANELS = [
        'ddt_request_history.panels.request_history.RequestHistoryPanel',
        'debug_toolbar.panels.versions.VersionsPanel',
        'debug_toolbar.panels.timer.TimerPanel',
        'debug_toolbar.panels.settings.SettingsPanel',
        'debug_toolbar.panels.headers.HeadersPanel',
        'debug_toolbar.panels.request.RequestPanel',
        'debug_toolbar.panels.sql.SQLPanel',
        'debug_toolbar.panels.staticfiles.StaticFilesPanel',
        'debug_toolbar.panels.templates.TemplatesPanel',
        'debug_toolbar.panels.cache.CachePanel',
        'debug_toolbar.panels.signals.SignalsPanel',
        'debug_toolbar.panels.logging.LoggingPanel',
        'debug_toolbar.panels.redirects.RedirectsPanel',
    ]

    # Logging
    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'standard': {
                'format': '%(levelname)-8s [%(asctime)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'standard',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO'
            },
            'celery': {
                'handlers': ['console'],
                'level': 'INFO'
            }
        }
    }

    JS_REVERSE_JS_MINIFY = False

    PY3 = sys.version_info[0] == 3
    if PY3:
        import builtins
    else:
        import __builtin__ as builtins

    try:
        builtins.profile
    except AttributeError:
        builtins.profile = lambda x: x


    class ConsoleErrorTracker():
        def captureException(self):
            print(traceback.format_exc())
            return -1

    ERROR_TRACKER = ConsoleErrorTracker()

else:
    SERVER_EMAIL = 'jun.ren.nz@gmail.com'
    # INSTALLED_APPS += ['corsheaders']
    MIDDLEWARE += ['corsheaders.middleware.CorsMiddleware', 'django.middleware.common.CommonMiddleware']
    CORS_ORIGIN_ALLOW_ALL = True

    DATABASES['default']['ATOMIC_REQUESTS'] = True

    # Security
    # SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
    # SECURE_SSL_REDIRECT = True
    # SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True
    SECURE_HSTS_SECONDS = 3600
    SECURE_HSTS_INCLUDE_SUBDOMAINS = True

    SECURE_CONTENT_TYPE_NOSNIFF = True
    SECURE_BROWSER_XSS_FILTER = True
    X_FRAME_OPTIONS = 'DENY'
    CSRF_COOKIE_HTTPONLY = True
    # Webpack
    WEBPACK_LOADER['DEFAULT']['CACHE'] = True

    # Whitenoise
    STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
    STATICFILES_DIRS = (
        base_dir_join('assets'),
    )

    MIDDLEWARE.insert(  # insert WhiteNoiseMiddleware right after SecurityMiddleware
        MIDDLEWARE.index('django.middleware.security.SecurityMiddleware') + 1,
        'whitenoise.middleware.WhiteNoiseMiddleware')

    # django-log-request-id
    MIDDLEWARE.insert(  # insert RequestIDMiddleware on the top
        0, 'log_request_id.middleware.RequestIDMiddleware')

    LOG_REQUEST_ID_HEADER = 'HTTP_X_REQUEST_ID'
    LOG_REQUESTS = True

    # Error tracker
    INSTALLED_APPS += ['raven.contrib.django.raven_compat', ]
    RAVEN_CONFIG = {
        'dsn': 'https://d79c30f5f7614914b50908ef8376cd41:e5caddeffcdb4c14bda9553e1ff2662c@sentry.io/1372284',
    }

    from raven.contrib.django.raven_compat.models import client

    ERROR_TRACKER = client

    LOGGING = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse'
            },
            'request_id': {
                '()': 'log_request_id.filters.RequestIDFilter'
            },
        },
        'formatters': {
            'standard': {
                'format': '%(levelname)-8s [%(asctime)s] [%(request_id)s] %(name)s: %(message)s'
            },
        },
        'handlers': {
            'null': {
                'class': 'logging.NullHandler',
            },
            'mail_admins': {
                'level': 'ERROR',
                'class': 'django.utils.log.AdminEmailHandler',
                'filters': ['require_debug_false'],
            },
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'filters': ['request_id'],
                'formatter': 'standard',
            },
        },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO'
            },
            'django.security.DisallowedHost': {
                'handlers': ['null'],
                'propagate': False,
            },
            'django.request': {
                'handlers': ['mail_admins'],
                'level': 'ERROR',
                'propagate': True,
            },
            'log_request_id.middleware': {
                'handlers': ['console'],
                'level': 'DEBUG',
                'propagate': False,
            },
        }
    }

    JS_REVERSE_EXCLUDE_NAMESPACES = ['admin']
