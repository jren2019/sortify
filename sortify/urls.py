from django.conf import settings
from django.urls import path, include
from django.conf.urls import include, url  # noqa
from django.contrib import admin
from django.views.generic import TemplateView

import django_js_reverse.views
from . import views
from django.conf.urls.static import static
# Uncomment the next two lines to enable the admin:
import xadmin
xadmin.autodiscover()

# version模块自动注册需要版本控制的 Model
from xadmin.plugins import xversion
xversion.register_models()

urlpatterns = [
    url(r'^react/$', TemplateView.as_view(template_name='exampleapp/itworks.html'), name='home_react'),

    path(r'', views.IndexView.as_view(), name='home'),
    path('posts/', include('apps.posts.urls'), name='posts'),

    url(r'^admin/', admin.site.urls),
    url(r'^jsreverse/$', django_js_reverse.views.urls_js, name='js_reverse'),

    # url(r'^$', TemplateView.as_view(template_name='exampleapp/itworks.html'), name='home'),

    # http://127.0.0.1:8000/api-auth/login/?name=superuser
    path(r'api-auth/', include('rest_framework.urls')),
    path(r'jsreverse/', django_js_reverse.views.urls_js, name='js_reverse'),
    path(r'xadmin/', xadmin.site.urls),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
