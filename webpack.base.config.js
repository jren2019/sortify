const path = require('path');

const nodeModulesDir = path.resolve(__dirname, 'node_modules');
const BundleTracker = require('webpack-bundle-tracker');

module.exports = [{
  entry: [
    './assets/js/jquery-index.js',
  ],
  output: {
    path: path.resolve('./assets/bundles/'),
    filename: 'bundle-jquery.js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: [nodeModulesDir],
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            cacheDirectory:true,
          },
        },
      },
      {
        test: /jquery\/dist\/jquery\.js$/,
        loader: 'expose-loader?$',
      },
      {
        test: /jquery\/dist\/jquery\.js$/,
        loader: 'expose-loader?jQuery',
      }],
  },
  plugins: [
    new BundleTracker({
      filename: './jquery-webpack-stats.json',
    }),
  ],
}, {
  context: __dirname,
  entry: [
    // defined in local or prod
  ],
  output: {
    // defined in local or prod
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        loaders: [
          'style-loader',
          'css-loader',
          'postcss-loader',
        ],
      },
      {
        test: /\.scss$/,
        loaders: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader',
        ],
      },
      {
         test: /\.less$/,
         use: [{
             loader: "style-loader",
         },{
             loader: "css-loader",
         },
         {
             loader:require.resolve('less-loader'),
             options: {
                 modules:false,
                 modifyVars:{
                     '@primary-color': '#f9c700',
                     '@link-color': '#f9c700',
                     '@border-radius-base': '2px',
                 },
                 javascriptEnabled: true,
             }
         }]
      },
      {
        test: /\.(svg)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader?limit=100000',
      },
      {
        test: /\.(jpg|png)?$/,
        loaders: [
          'file-loader?name=i-[hash].[ext]',
        ],
      },
    ],
  },
  plugins: [
    // defined in local or prod
      ["import", { "libraryName": "antd", "style": true }], // `style: true` for less
  ],
  resolve: {
    modules: [
      'node_modules',
      'bower_components',
      path.resolve(__dirname, 'assets/js/'),
    ],
    extensions: ['.js', '.jsx'],
  },
}];
